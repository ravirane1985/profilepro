'use strict';
var db = require('../lib/database');


module.exports = function spec(app) {

	return {
		onconfig: function(config, next) {

			//configure mongodb and paypal sdk
			db.config(config.get('databaseConfig'));

          //  userLib.addUsers();
			next(null, config);
		}
	};

};