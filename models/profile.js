'use strict';

var mongoose = require('mongoose');

var profileModel = function() {

    var profileSchema = mongoose.Schema({
    	profileId: { type: String, unique: true },
    	profilePic: String,
    	profileName: String,
    	aboutMe: String,
    	profileDetails1: String,
    	profileDetails2: String,
    	contactMe:{
    		mailId:String,
    		faceBookProfile:String,
    		linkedInProfile:String
    	}
    });

    return mongoose.model('Profile', profileSchema);

};

module.exports = new profileModel();